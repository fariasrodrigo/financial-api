package com.financial.rest.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.financial.dto.request.MethodRequest;
import com.financial.dto.response.MethodResponse;
import com.financial.service.MethodService;
import com.financial.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class MethodResourceTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MethodService service;

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void createShouldReturnMethodFromService() throws Exception {

        // given
        MethodRequest request = new MethodRequest("method_1");
        MethodResponse response = new MethodResponse(1L, "method_1");

        // when
        when(service.create(any(MethodRequest.class))).thenReturn(response);

        // then
        mockMvc.perform(post("/api/v1/methods")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(response.getId()))
                .andExpect(jsonPath("$.name").value(response.getName()));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUserShouldReturnAccount() throws Exception {

        // given
        MethodResponse response = new MethodResponse(2L, "method_2");

        // when
        when(service.findByIdAndUser(anyLong())).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/methods/{id}", 2L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("method_2"));

        verify(service, times(1)).findByIdAndUser(2L);
        verifyNoMoreInteractions(service);
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUser_ShouldReturnHttpStatusCode404() throws Exception {

        //when
        when(service.findByIdAndUser(anyLong())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found."));

        // then
        this.mockMvc
                .perform(get("/api/v1/methods/{7}", 7L))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findAllByLoggedUserShouldReturnAccountList() throws Exception {

        // given
        List<MethodResponse> response = new ArrayList<>();
        MethodResponse a1 = new MethodResponse(3L, "method_3");
        MethodResponse a2 = new MethodResponse(4L, "method_4");
        response.add(a1);
        response.add(a2);

        // when
        when(service.findAllByLoggedUser()).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/methods"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(3))
                .andExpect(jsonPath("$[0].name").value("method_3"))
                .andExpect(jsonPath("$[1].id").value(4))
                .andExpect(jsonPath("$[1].name").value("method_4"));

    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void update_ShouldReturnUpdatedMethod() throws Exception {

        // given
        MethodRequest request = new MethodRequest("old method");
        MethodResponse response = new MethodResponse(3L, "method updated");


        // when
        when(service.update(anyLong(), any(MethodRequest.class))).thenReturn(response);

        // then
        mockMvc.perform(put("/api/v1/methods/{id}", 3L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()));
    }


}

