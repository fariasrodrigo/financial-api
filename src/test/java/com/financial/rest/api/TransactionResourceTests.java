/*
package com.financial.rest.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.financial.dto.request.TransactionRequest;
import com.financial.dto.response.TransactionResponse;
import com.financial.enumerations.TransactionType;
import com.financial.service.TransactionService;
import com.financial.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class TransactionResourceTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionService service;


    private UserService userService;


    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void createShouldReturnTransactionFromService() throws Exception {

        // given
        TransactionRequest request = new TransactionRequest(BigDecimal.TEN, "2020-12-09", "2020-12-09",true, "Transaction_1",  );
        TransactionResponse response = new TransactionResponse(1L, BigDecimal.TEN, LocalDate.now(), true, "Transaction_1", TransactionType.EXPENSE, );

        // when
        when(service.create(any(TransactionRequest.class))).thenReturn(response);

        // then
        mockMvc.perform(post("/api/v1/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(response.getName()))
                .andExpect(jsonPath("$.transactionType").value("EXPENSE"))
                .andExpect(jsonPath("$.amount").value(response.getAmount()))
                .andExpect(jsonPath("$.paymentDate").value("2020-11-23"));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUserShouldReturnTransaction() throws Exception {

        // given
        TransactionResponse response = new TransactionResponse(2L, "transaction_2", TransactionType.EXPENSE, BigDecimal.TEN, LocalDate.now());

        // when
        when(service.findByIdAndUser(anyLong())).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/transactions/{id}", 2L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()))
                .andExpect(jsonPath("$.amount").value(response.getAmount()))
                .andExpect(jsonPath("transactionType").value(response.getTransactionType()))
                .andExpect(jsonPath("paymentDate").value(response.getPaymentDate()));
        verify(service, times(1)).findByIdAndUser(2L);
        verifyNoMoreInteractions(service);


    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUser_ShouldReturnHttpStatusCode404() throws Exception {

        //when
        when(service.findByIdAndUser(anyLong())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found."));

        // then
        this.mockMvc
                .perform(get("/api/v1/transactions/{8}", 8L))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findAllByLoggedUserShouldReturnTransactiontList() throws Exception {

        // given
        List<TransactionResponse> response = new ArrayList<>();
        TransactionResponse a1 = new TransactionResponse(3L, "transaction_3", TransactionType.EXPENSE, BigDecimal.TEN, LocalDate.now());
        TransactionResponse a2 = new TransactionResponse(4L, "transaction_4", TransactionType.INCOME, BigDecimal.ZERO, LocalDate.now());

        response.add(a1);
        response.add(a2);

        // when
        when(service.findAllByLoggedUser()).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/transactions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(a1.getId()))
                .andExpect(jsonPath("$[0].name").value(a1.getName()))
                .andExpect(jsonPath("$[0].transactionType").value("EXPENSE"))
                .andExpect(jsonPath("$[0].amount").value(a1.getAmount()))
                .andExpect(jsonPath("$[0].paymentDate").value("2020-11-23"))
                .andExpect(jsonPath("$[1].id").value(a2.getId()))
                .andExpect(jsonPath("$[1].name").value(a2.getName()))
                .andExpect(jsonPath("$[1].transactionType").value("INCOME"))
                .andExpect(jsonPath("$[1].amount").value(a2.getAmount()))
                .andExpect(jsonPath("$[1].paymentDate").value("2020-11-23"));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void update_ShouldReturnUpdatedTransaction() throws Exception {

        TransactionRequest request = new TransactionRequest("transaction_3", TransactionType.EXPENSE, BigDecimal.TEN, LocalDate.now());
        TransactionResponse response = new TransactionResponse(5L, "transaction_updated", TransactionType.INCOME, BigDecimal.TEN, LocalDate.now());

        // when
        when(service.update(anyLong(), any(TransactionRequest.class))).thenReturn(response);


        // then
        mockMvc.perform(put("/api/v1/categories/{id}", 5L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()));
    }



}

*/
