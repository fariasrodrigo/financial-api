package com.financial.rest.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.financial.dto.request.AccountRequest;
import com.financial.dto.response.AccountResponse;
import com.financial.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class AccountResourceTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService service;

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void createShouldReturnAccountFromService() throws Exception {

        // given
        AccountRequest request = new AccountRequest("account", BigDecimal.TEN);
        AccountResponse response = new AccountResponse(1L, "account", BigDecimal.TEN, BigDecimal.TEN);

        // when
        when(service.create(any(AccountRequest.class))).thenReturn(response);

        // then
        mockMvc.perform(post("/api/v1/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(response.getId()))
                .andExpect(jsonPath("$.name").value(response.getName()))
                .andExpect(jsonPath("$.initialBalance").value(response.getInitialBalance()));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findAllByLoggedUserShouldReturnAccountList() throws Exception {

        // given
        List<AccountResponse> response = new ArrayList<>();
        AccountResponse a1 = new AccountResponse(1L, "account_1", BigDecimal.TEN, BigDecimal.TEN);
        AccountResponse a2 = new AccountResponse(2L, "account_2", BigDecimal.ONE, BigDecimal.TEN);
        response.add(a1);
        response.add(a2);

        // when
        when(service.findAllByLoggedUser()).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/accounts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(a1.getId()))
                .andExpect(jsonPath("$[0].name").value(a1.getName()))
                .andExpect(jsonPath("$[0].initialBalance").value(a1.getInitialBalance()))
                .andExpect(jsonPath("$[1].id").value(a2.getId()))
                .andExpect(jsonPath("$[1].name").value(a2.getName()))
                .andExpect(jsonPath("$[1].initialBalance").value(a2.getInitialBalance()));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUserShouldReturnAccount() throws Exception {

        // given
        AccountResponse response = new AccountResponse(3L, "account_3", BigDecimal.ONE, BigDecimal.TEN);

        // when
        when(service.findByIdAndUser(anyLong())).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/accounts/{id}", 3L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()))
                .andExpect(jsonPath("$.initialBalance").value(response.getInitialBalance()));
        verify(service, times(1)).findByIdAndUser(3L);
        verifyNoMoreInteractions(service);


    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUser_ShouldReturnHttpStatusCode404() throws Exception {

        //when
        when(service.findByIdAndUser(anyLong())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found."));

        // then
        this.mockMvc
                .perform(get("/api/v1/accounts/{5}", 5L))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void update_ShouldReturnUpdatedAccount() throws Exception {

        AccountRequest request = new AccountRequest("account", BigDecimal.ZERO);
        AccountResponse response = new AccountResponse(4L, "account updated", BigDecimal.TEN, BigDecimal.TEN);

        // when
        when(service.update(anyLong(), any(AccountRequest.class))).thenReturn(response);


        // then
        mockMvc.perform(put("/api/v1/accounts/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()))
                .andExpect(jsonPath("$.initialBalance").value(response.getInitialBalance()));
    }

}








