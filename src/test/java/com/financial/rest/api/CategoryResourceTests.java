package com.financial.rest.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.financial.dto.request.CategoryRequest;
import com.financial.dto.response.CategoryResponse;
import com.financial.service.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class CategoryResourceTests {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService service;

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void createShouldReturnCategoryFromService() throws Exception {

        // given
        CategoryRequest request = new CategoryRequest("category");
        CategoryResponse response = new CategoryResponse(1L, "category");

        // when
        when(service.create(any(CategoryRequest.class))).thenReturn(response);

        // then
        mockMvc.perform(post("/api/v1/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(response.getId()))
                .andExpect(jsonPath("$.name").value(response.getName()));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUserShouldReturnCategory() throws Exception {

        // given
        CategoryResponse response = new CategoryResponse(3L, "category_3");

        // when
        when(service.findByIdAndUser(anyLong())).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/categories/{id}", 3L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()));
        verify(service, times(1)).findByIdAndUser(3L);
        verifyNoMoreInteractions(service);


    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findByIdAndUser_ShouldReturnHttpStatusCode404() throws Exception {

        //when
        when(service.findByIdAndUser(anyLong())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found."));

        // then
        this.mockMvc
                .perform(get("/api/v1/categories/{5}", 5L))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void findAllByLoggedUserShouldReturnAccountList() throws Exception {

        // given
        List<CategoryResponse> response = new ArrayList<>();
        CategoryResponse a1 = new CategoryResponse(1L, "category_1");
        CategoryResponse a2 = new CategoryResponse(2L, "category_2");
        response.add(a1);
        response.add(a2);

        // when
        when(service.findAllByLoggedUser()).thenReturn(response);

        // then
        this.mockMvc
                .perform(get("/api/v1/categories"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(a1.getId()))
                .andExpect(jsonPath("$[0].name").value(a1.getName()))
                .andExpect(jsonPath("$[1].id").value(a2.getId()))
                .andExpect(jsonPath("$[1].name").value(a2.getName()));
    }

    @Test
    @WithMockUser(username = "test@test", roles = {"USER", "ADMIN"})
    public void update_ShouldReturnUpdatedAccount() throws Exception {

        CategoryRequest request = new CategoryRequest("category");
        CategoryResponse response = new CategoryResponse(4L, "category updated");

        // when
        when(service.update(anyLong(), any(CategoryRequest.class))).thenReturn(response);


        // then
        mockMvc.perform(put("/api/v1/categories/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(response.getName()));
    }


}
