package com.financial.mapper;

import com.financial.dto.response.MethodResponse;
import com.financial.model.Method;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MethodMapper {

    public MethodResponse toResponse(Method method) {
        return new MethodResponse(
                method.getId(),
                method.getName());
    }

    public List<MethodResponse> toList(List<Method> objects) {
        List<MethodResponse> methods = new ArrayList<>();

        objects.forEach(obj -> {
            methods.add(this.toResponse(obj));
        });
        return methods;
    }
}
