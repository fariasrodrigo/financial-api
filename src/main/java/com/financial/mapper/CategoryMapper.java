package com.financial.mapper;

import com.financial.dto.response.CategoryResponse;
import com.financial.model.Category;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryMapper {

    public CategoryResponse toResponse(Category category) {
        return new CategoryResponse(
                category.getId(),
                category.getName());
    }

    public List<CategoryResponse> toList(List<Category> objects) {
        List<CategoryResponse> categories = new ArrayList<>();

        objects.forEach(obj -> {
            categories.add(this.toResponse(obj));
        });
        return categories;
    }
}
