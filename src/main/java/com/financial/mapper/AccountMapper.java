package com.financial.mapper;

import com.financial.dto.response.AccountResponse;
import com.financial.model.Account;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountMapper {

    public AccountResponse toResponse(Account account) {
        return new AccountResponse(
                account.getId(),
                account.getName(),
                account.getInitialBalance(),
                account.getCurrentBalance());
    }

    public List<AccountResponse> toList(List<Account> objects) {
        List<AccountResponse> accounts = new ArrayList<>();

        objects.forEach(obj -> {
            accounts.add(this.toResponse(obj));
        });
        return accounts;
    }
}
