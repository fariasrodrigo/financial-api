package com.financial.mapper;

import com.financial.dto.response.UserResponse;
import com.financial.model.Role;
import com.financial.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserMapper {

    public UserResponse toResponse(User user) {
        return new UserResponse(
                user.getId(),
                user.getName(),
                user.getUsername(),
                getRoles(user));
    }

    public List<UserResponse> toList(List<User> objects) {
        List<UserResponse> users = new ArrayList<>();

        objects.forEach(obj -> {
            users.add(this.toResponse(obj));
        });
        return users;
    }

    private Set<String> getRoles(User user) {
        return user.getRoles().stream().map(Role::getName).collect(Collectors.toSet());
    }
}
