package com.financial.mapper;

import com.financial.dto.response.TransactionResponse;
import com.financial.model.Transaction;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionMapper {

    public TransactionResponse toResponse(Transaction transaction) {
        return new TransactionResponse(
                transaction.getId(),
                transaction.getAmount(),
                transaction.getCreated(),
                transaction.getIsPaid(),
                transaction.getName(),
                transaction.getTransactionType(),
                transaction.getPaymentDate(),
                transaction.getAccount(),
                transaction.getCategory(),
                transaction.getMethod(),
                transaction.getUser());

    }

    public List<TransactionResponse> toList(List<Transaction> objects) {
        List<TransactionResponse> transactions = new ArrayList<>();

        objects.forEach(obj -> {
            transactions.add(this.toResponse(obj));
        });
        return transactions;
    }
}
