package com.financial.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;


@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "tbl_account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter
    @NotNull @NotEmpty
    @Column(name = "name")
    private String name;

    @JsonIgnore
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @JsonIgnore
    @Setter
    @NotNull @NotEmpty
    @Column(name = "initial_balance", precision = 10, scale = 2)
    private BigDecimal initialBalance = BigDecimal.ZERO;

    @JsonIgnore
    @ManyToOne
    private User user;

    // Transiet
    @Setter
    @Transient
    private BigDecimal currentBalance = BigDecimal.ZERO;

    // Constructors

    public Account(@NotEmpty String name, @NotEmpty BigDecimal initialBalance, User user) {
        this.name = name;
        this.initialBalance = initialBalance;
        this.user = user;
    }
}
