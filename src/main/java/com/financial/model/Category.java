package com.financial.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "tbl_category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter
    @Column(name = "name")
    private String name;

    @JsonIgnore
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @JsonIgnore
    @ManyToOne
    private User user;

    //Constructor

    public Category(String name, User user) {
        this.name = name;
        this.user = user;
    }
}
