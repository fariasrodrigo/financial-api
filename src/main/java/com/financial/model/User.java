package com.financial.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "tbl_user")
public class User {

    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @Getter
    @Column(name = "username", length = 60, unique = true, nullable = false)
    private String username;

    @JsonIgnore
    @Getter
    @Column(name = "password", length = 60, nullable = false)
    private String password;

    @JsonIgnore
    @Getter
    @Column(name = "first_name")
    private String firstName;

    @JsonIgnore
    @Getter
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "activated")
    private boolean activated;

    @JsonIgnore
    @Getter
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private Set<Role> roles;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Account> accounts;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Method> methods;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Category> categories;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Transaction> transactions;

    // Constructors

    public User(String username) {
        this.username = username;
    }

    // methods

    public String getName() {
        return this.getFirstName() + " " + this.getLastName();
    }

}
