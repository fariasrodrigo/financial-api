package com.financial.model;

import com.financial.enumerations.TransactionType;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "tbl_transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter
    @NotNull
    @Column(name = "amount", precision = 10, scale = 2)
    private BigDecimal amount = BigDecimal.ZERO;

    @Column(name = "created")
    private LocalDate created = LocalDate.now();

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "is_paid")
    private Boolean isPaid = false;

    @Setter
    @Column(name = "name")
    private String name;

    @Setter
    @Column(name = "payment_date")
    private LocalDate paymentDate;

    @Setter
    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @OneToOne
    private Account account;

    @OneToOne
    private Category category;

    @OneToOne
    private Method method;

    @ManyToOne
    private User user;

    // Constructors

    public Transaction(@NotEmpty BigDecimal amount, LocalDate expirationDate, boolean isPaid, @NotEmpty String name, LocalDate paymentDate,@NotEmpty TransactionType transactionType, Account account, @NotEmpty Category category, @NotEmpty Method method) {
        this.amount = amount;
        this.expirationDate = expirationDate;
        this.isPaid = isPaid;
        this.name = name;
        this.paymentDate = paymentDate;
        this.transactionType = transactionType;
        this.account = account;
        this.category = category;
        this.method = method;
    }

}
