package com.financial.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "tbl_method")
public class Method {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter
    @Column(name = "name")
    private String name;

    @JsonIgnore
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @JsonIgnore
    @ManyToOne
    private User user;

    //Constructor

    public Method(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public Method(Long id, String name, User user) {
        this.id = id;
        this.name = name;
        this.user = user;
    }

    public Method(Long id, String name) {

    }
}
