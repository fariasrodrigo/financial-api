package com.financial.rest.authentication;

import com.financial.dto.request.AuthRequest;
import com.financial.dto.response.AuthResponse;
import com.financial.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthenticationResource {

    private final AuthenticationService service;

    @Autowired
    public AuthenticationResource(AuthenticationService service) {
        this.service = service;
    }

    @PostMapping(value = "/authenticate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthResponse authenticate(@RequestBody AuthRequest credentialRequest, HttpServletRequest request) {
        return service.authenticate(credentialRequest, request);
    }
}
