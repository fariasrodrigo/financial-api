package com.financial.rest.api;

import com.financial.dto.request.CategoryRequest;
import com.financial.dto.response.CategoryResponse;
import com.financial.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/categories")
public class CategoryResource {

    private final CategoryService categoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryResponse> findAllByLoggedUser() throws Exception {
        return categoryService.findAllByLoggedUser();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryResponse findByIdAndUser(@RequestBody @PathVariable Long id) throws Exception {
        return categoryService.findByIdAndUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CategoryResponse create(@RequestBody CategoryRequest request) throws Exception {
        return categoryService.create(request);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryResponse update(@PathVariable Long id, @RequestBody CategoryRequest request) throws Exception {
        return categoryService.update(id, request);
    }
}
