package com.financial.rest.api;

import com.financial.dto.request.MethodRequest;
import com.financial.dto.response.MethodResponse;
import com.financial.service.MethodService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/methods")
public class MethodResource {

    private final MethodService methodService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<MethodResponse> findAllByLoggedUser() throws Exception {
        return methodService.findAllByLoggedUser();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MethodResponse findByIdAndUser(@PathVariable Long id) throws Exception {
        return methodService.findByIdAndUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MethodResponse create(@RequestBody MethodRequest request) throws Exception {
        return methodService.create(request);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MethodResponse update(@PathVariable Long id, @RequestBody MethodRequest request) throws Exception {
        return methodService.update(id, request);
    }
}
