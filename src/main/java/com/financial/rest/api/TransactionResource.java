package com.financial.rest.api;

import com.financial.dto.request.TransactionRequest;
import com.financial.dto.response.TransactionResponse;
import com.financial.service.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/transactions")
public class TransactionResource {

    private final TransactionService transactionService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TransactionResponse create(@RequestBody TransactionRequest request) throws Exception {
        return transactionService.create(request);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse findByIdAndUser (@RequestBody @PathVariable Long id) throws Exception {
        return transactionService.findByIdAndUser(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TransactionResponse> findAllByLoggedUser() throws Exception {
        return transactionService.findAllByLoggedUser();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse update (@PathVariable  Long id, @RequestBody TransactionRequest request) throws Exception {
        return transactionService.update(id, request);
    }



}
