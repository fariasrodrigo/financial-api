package com.financial.rest.api;

import com.financial.dto.request.AccountRequest;
import com.financial.dto.response.AccountResponse;
import com.financial.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/accounts")
public class AccountResource {

    private final AccountService accountService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<AccountResponse> findAllByLoggedUser() throws Exception {
        return accountService.findAllByLoggedUser();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AccountResponse findByIdAndUser(@RequestBody @PathVariable Long id) throws Exception {
        return accountService.findByIdAndUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AccountResponse create(@RequestBody AccountRequest request) throws Exception {
        return accountService.create(request);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AccountResponse update(@PathVariable Long id, @RequestBody AccountRequest request) throws Exception {
        return accountService.update(id, request);
    }
}
