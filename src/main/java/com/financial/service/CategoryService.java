package com.financial.service;

import com.financial.dto.request.CategoryRequest;
import com.financial.dto.response.CategoryResponse;
import com.financial.mapper.CategoryMapper;
import com.financial.model.Category;
import com.financial.model.User;
import com.financial.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@AllArgsConstructor
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final UserService userService;
    private final CategoryMapper mapper;

    public List<CategoryResponse> findAllByLoggedUser() throws Exception {
        try {
            User user = userService.getLoggedUser();
            List<Category> category = categoryRepository.findAllByUser(user);
            return mapper.toList(category);
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }

    public CategoryResponse findByIdAndUser(Long id) {
        User user = userService.getLoggedUser();
        try {
            Category category = categoryRepository.findByIdAndUser(id, user);
            return mapper.toResponse(category);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Category has not been found.");
        }
    }

    public CategoryResponse create(CategoryRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Category category = new Category(request.getName(), user);
            return mapper.toResponse(categoryRepository.save(category));
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }

    public CategoryResponse update(Long id, CategoryRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Category category = categoryRepository.findByIdAndUser(id, user);
            category.setName(request.getName());
            Category categoryUpdated = categoryRepository.save(category);
            return mapper.toResponse(categoryUpdated);
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }
}
