package com.financial.service;

import com.financial.dto.request.TransactionRequest;
import com.financial.dto.response.TransactionResponse;
import com.financial.enumerations.TransactionType;
import com.financial.mapper.TransactionMapper;
import com.financial.model.Account;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repository.TransactionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final TransactionMapper mapper;
    private final UserService userService;


    public TransactionResponse create(TransactionRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();

        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
        Transaction transaction = new Transaction(request.getAmount(), request.getExpirationDate(), request.getIsPaid(),
                request.getName(), request.getPaymentDate(), request.getTransactionType(), request.getAccount(), request.getCategory(), request.getMethod());
        return mapper.toResponse(transactionRepository.save(transaction));
    }


    public TransactionResponse findByIdAndUser(Long id) throws Exception {
        User user = userService.getLoggedUser();
        try {
            Transaction transaction = transactionRepository.findByIdAndUser(id, user);
            return mapper.toResponse(transaction);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Transaction has not been found.");
        }
    }

    public List<TransactionResponse> findAllByLoggedUser() throws Exception {
        try {
            User user = userService.getLoggedUser();
            List<Transaction> transaction = transactionRepository.findAllByUser(user);
            return mapper.toList(transaction);
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }

    public TransactionResponse update(Long id, TransactionRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Transaction transaction = transactionRepository.findByIdAndUser(id, user);
            transaction.setName(request.getName());
            transaction.setTransactionType(request.getTransactionType());
            transaction.setAmount(request.getAmount());
            transaction.setPaymentDate(request.getPaymentDate());
            Transaction transactionUpdated = transactionRepository.save(transaction);
            return mapper.toResponse(transactionUpdated);

        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }


    }

    public BigDecimal calculateIncome(Account account) {
        List<Transaction> transactions = transactionRepository.findByAccount(account);
        BigDecimal income = transactions
                .stream()
                .filter(f -> f.getTransactionType().equals(TransactionType.INCOME))
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return income;
    }

    public BigDecimal calculateExpense(Account account) {
        List<Transaction> transactions = transactionRepository.findByAccount(account);
        BigDecimal expense = transactions
                .stream()
                .filter(f -> f.getTransactionType().equals(TransactionType.EXPENSE))
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return expense;
    }

    public BigDecimal currentBalance(Account account) {
        BigDecimal income = calculateIncome(account);
        BigDecimal expense = calculateExpense(account);
        BigDecimal currentBalance = income.subtract(expense).add(account.getInitialBalance());

        return currentBalance;
    }

}

