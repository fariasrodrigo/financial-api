package com.financial.service;

import com.financial.dto.request.AccountRequest;
import com.financial.dto.response.AccountResponse;
import com.financial.mapper.AccountMapper;
import com.financial.model.Account;
import com.financial.model.User;
import com.financial.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper mapper;
    private final UserService userService;
    private final TransactionService transactionService;

    public List<AccountResponse> findAllByLoggedUser() throws Exception {
        try {
            User user = userService.getLoggedUser();
            List<Account> accounts = accountRepository.findAllByUser(user);
            accounts.forEach(account -> {
             BigDecimal currentBalance = transactionService.currentBalance(account);
                account.setCurrentBalance(currentBalance);
            });

            return mapper.toList(accounts);
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }


    public AccountResponse findByIdAndUser(Long id) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Account account = accountRepository.findByIdAndUser(id, user);
            BigDecimal currentBalance = transactionService.currentBalance(account);
            account.setCurrentBalance(currentBalance);

            return mapper.toResponse(account);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Account has not been found.");

        }
    }

    public AccountResponse create(AccountRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Account account = new Account(request.getName(), request.getInitialBalance(), user);
            return mapper.toResponse(accountRepository.save(account));
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }

    public AccountResponse update(Long id, AccountRequest request) throws Exception {
        User user = userService.getLoggedUser();
        try {
            Account account = accountRepository.findByIdAndUser(id, user);
            account.setName(request.getName());
            account.setInitialBalance(request.getInitialBalance());
            Account accountUpdated = accountRepository.save(account);
            return mapper.toResponse(accountUpdated);
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }
}
