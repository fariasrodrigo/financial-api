package com.financial.service;

import com.financial.dto.request.MethodRequest;
import com.financial.dto.response.MethodResponse;
import com.financial.mapper.MethodMapper;
import com.financial.model.Method;
import com.financial.model.User;
import com.financial.repository.MethodRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@AllArgsConstructor
@Service
public class MethodService {

    private final MethodRepository methodRepository;
    private final UserService userService;
    private final MethodMapper mapper;

    public List<MethodResponse> findAllByLoggedUser() throws Exception {
        try {
            User user = userService.getLoggedUser();
            List<Method> method = methodRepository.findAllByUser(user);
            return mapper.toList(method);
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }

    public MethodResponse findByIdAndUser(Long id) {
        User user = userService.getLoggedUser();
        try {
            Method method = methodRepository.findByIdAndUser(id, user);
            return mapper.toResponse(method);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Method has not been found.");
        }
    }

    public MethodResponse create(MethodRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Method method = new Method(request.getName(), user);
            return mapper.toResponse(methodRepository.save(method));
        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }

    public MethodResponse update(Long id, MethodRequest request) throws Exception {
        try {
            User user = userService.getLoggedUser();
            Method method = methodRepository.findByIdAndUser(id, user);
            method.setName(request.getName());
            Method methodUpdated = methodRepository.save(method);
            return mapper.toResponse(methodUpdated);

        } catch (Exception e) {
            throw new Exception("A server error has occurred.", e);
        }
    }
}
