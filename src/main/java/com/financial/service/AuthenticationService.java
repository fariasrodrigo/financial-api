package com.financial.service;

import com.financial.configuration.DomainUserDetailsService;
import com.financial.configuration.TokenGenerator;
import com.financial.dto.request.AuthRequest;
import com.financial.dto.response.AuthResponse;
import com.financial.dto.response.UserResponse;
import com.financial.mapper.UserMapper;
import com.financial.model.User;
import com.financial.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

@Service
public class AuthenticationService {

    private final AuthenticationManagerBuilder auth;
    private final DomainUserDetailsService userDetailsService;
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationService(AuthenticationManagerBuilder auth,
                                 DomainUserDetailsService userDetailsService,
                                 UserRepository userRepository,
                                 UserMapper userMapper) {
        this.auth = auth;
        this.userDetailsService = userDetailsService;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public AuthResponse authenticate(AuthRequest authRequest, HttpServletRequest request) {
        User user = userRepository.findOneByUsernameIgnoreCase(authRequest.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User not found!"));

        BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
        if (!bCrypt.matches(authRequest.getPassword(), user.getPassword())) {
            throw new UsernameNotFoundException("Incorrect password!");
        }

        UsernamePasswordAuthenticationToken authentication = setUserInContext(user.getUsername(), request);

        if (authentication.isAuthenticated()) {
            UserResponse userResponse = userMapper.toResponse(user);
            String token = new TokenGenerator().generateToken(userResponse);
            return new AuthResponse(token);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "An error occurred while trying to authenticate.");
        }
    }

    private UsernamePasswordAuthenticationToken setUserInContext(String username, HttpServletRequest request) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                userDetails,
                null,
                userDetails.getAuthorities());

        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication;
    }
}
