package com.financial.enumerations;

public enum TransactionType {

    INCOME, EXPENSE;
}
