package com.financial.dto.response;

import com.financial.enumerations.TransactionType;
import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import com.financial.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionResponse {

    private Long id;
    private BigDecimal amount = BigDecimal.ZERO;
    private LocalDate created;
    private Boolean isPaid;
    private String name;
    private TransactionType transactionType;
    private LocalDate paymentDate;
    private Account Account;
    private Category Category;
    private Method method;
    private User user;


}

