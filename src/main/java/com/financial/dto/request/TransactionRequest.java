package com.financial.dto.request;

import com.financial.enumerations.TransactionType;
import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRequest {

    private BigDecimal amount;
    private LocalDate expirationDate;
    private Boolean isPaid;
    private String name;
    private LocalDate paymentDate;
    private TransactionType transactionType;
    private Account account;
    private Category category;
    private Method method;

}
