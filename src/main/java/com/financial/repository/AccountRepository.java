package com.financial.repository;

import com.financial.model.Account;
import com.financial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> findAllByUser(User user);

    Account findByIdAndUser(Long id, User user);

}
