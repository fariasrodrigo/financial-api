package com.financial.repository;

import com.financial.model.Method;
import com.financial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MethodRepository extends JpaRepository<Method, Long> {
    Method findByIdAndUser(Long id, User user);

    List<Method> findAllByUser(User user);
}
