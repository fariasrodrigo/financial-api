package com.financial.repository;

import com.financial.model.Account;
import com.financial.model.Transaction;
import com.financial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Transaction findByIdAndUser (Long id, User user);

    List<Transaction> findByAccount (Account account);

    List<Transaction> findAllByUser(User user);




}
