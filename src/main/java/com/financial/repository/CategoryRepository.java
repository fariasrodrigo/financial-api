package com.financial.repository;

import com.financial.model.Category;
import com.financial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findByIdAndUser(Long id, User user);

    List<Category> findAllByUser(User user);
}
