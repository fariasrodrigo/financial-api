create table if not exists tbl_user
(
    id         bigint auto_increment primary key,
    first_name varchar(50)  not null,
    last_name  varchar(50)  not null,
    username   varchar(100) not null,
    password   varchar(100) not null,
    activated  bit          not null,
    constraint UK_k0bty7tbcye41jpxam88q5kj2 unique (username)
);

create table if not exists tbl_role
(
    id      int auto_increment primary key,
    name    varchar(50) not null,
    user_id bigint      null,
    constraint FK73mvekd5kf1a88gxsh0714ysn foreign key (user_id) references tbl_user (id)
);











