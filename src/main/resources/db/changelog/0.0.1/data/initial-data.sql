insert into tbl_user (first_name, last_name, username, password, activated)
values ('Admin', 'Admin', 'admin@gmail.com', '$2a$10$uhMBC0QLCXoWXbwGnZye0u6SeFdwEvmm5RTanIXpFGHwC33FqS9Rm', true),
       ('User', 'User', 'user@gmail.com', '$2a$10$uhMBC0QLCXoWXbwGnZye0u6SeFdwEvmm5RTanIXpFGHwC33FqS9Rm', true);

insert into tbl_role (name, user_id)
values ('ROLE_ADMIN', 1),
       ('ROLE_USER', 2);





