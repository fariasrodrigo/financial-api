create table if not exists tbl_method
(
	id bigint auto_increment primary key,
	is_deleted bit null,
	name varchar(255) null,
	user_id bigint null,
	constraint FKl2alk725h21txyrvswqml4hj0 foreign key (user_id) references tbl_user (id)
);

create table if not exists tbl_category
(
	id bigint auto_increment primary key,
	is_deleted bit null,
	name varchar(255) null,
	user_id bigint null,
	constraint FKhrb3or77f746kuju88mdvbn62 foreign key (user_id) references tbl_user (id)
);

create table if not exists tbl_account
(
	id bigint auto_increment primary key,
	initial_balance decimal(10,2) null,
	is_deleted bit null,
	name varchar(255) null,
	user_id bigint null,
	constraint FKnd41d2s8gtijiuqfm61q03ohw foreign key (user_id) references tbl_user (id)
);

create table if not exists tbl_transaction
(
	id bigint auto_increment primary key,
	amount decimal(10,2) null,
	created datetime null,
	expiration_date date null,
	is_paid bit null,
	name varchar(255) null,
	payment_date date null,
	transaction_type int null,
	account_id bigint null,
	category_id bigint null,
	method_id bigint null,
	user_id bigint null,
	constraint FK6g8tqghnt1p2bja76p6kev9x6 foreign key (category_id) references tbl_category (id),
	constraint FKg9trefncspvhnejqhejupuifk foreign key (method_id) references tbl_method (id),
	constraint FKhexjssdi43dghefat1uoscm9y foreign key (account_id) references tbl_account (id),
	constraint FKt0nwe4k7ev80mjgvte45e660 foreign key (user_id) references tbl_user (id)
);











