insert into tbl_account (name, is_deleted, initial_balance, user_id)
values ('Market Team', false , 450, 1),
       ('Comercial Team', false , 1000, 1);

insert into tbl_category  (name, is_deleted, user_id)
values ('Market', false, 1),
       ('Comercial', false , 1);

insert into tbl_method (name, is_deleted, user_id)
values ('Paypal', false, 1),
       ('Transfer', false, 1);

insert into tbl_transaction (amount, created, expiration_date, is_paid, name, payment_date, transaction_type, account_id, category_id, method_id, user_id)
values ('200.00', '2020-12-07', '2020-07-12', true, "Market Expenses", '2020-07-12', 1, 1, 1, 1, 1),
       ('600.00', '2020-07-12' , '2020-07-12', true, "Trend Payment Income", '2020-07-12', 1, 1, 2, 2, 1);




